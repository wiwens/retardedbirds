//
//  GroundNode.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 11.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class GroundNode {
    
    class func createGround(frame: CGRect) -> SKSpriteNode {
        
        let ground = SKSpriteNode(imageNamed: "Ground")
        ground.setScale(0.5)
        ground.position = CGPoint(x: frame.width / 2, y: 0 + ground.frame.height / 2)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: ground.size)
        ground.physicsBody?.categoryBitMask = PhysicsCategory.Ground
        ground.physicsBody?.collisionBitMask = PhysicsCategory.Bird
        ground.physicsBody?.contactTestBitMask = PhysicsCategory.Bird
        ground.physicsBody?.affectedByGravity = false
        ground.physicsBody?.dynamic = false
        ground.zPosition = 3
        
        return ground
    }
}
