//
//  ScoreLabelNode.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 11.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class ScoreLabelNode: SKLabelNode {
    
    init(frame: CGRect) {
        super.init()
    
        createScoreLabel(frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createScoreLabel(frame: CGRect) {
        
        self.text = "0"
        self.position = CGPoint(x: frame.width / 2, y: frame.height / 2 + frame.height / 2.5)
        self.zPosition = 5
    }
}

extension SKLabelNode {
    
    func updateScore(score: Int) {
        
        self.text = "\(score)"
    }
}