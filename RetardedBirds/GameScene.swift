//
//  GameScene.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 10.01.2016.
//  Copyright (c) 2016 Wiktor. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var ground: SKSpriteNode!
    var bird: SKSpriteNode!
    var scoreLabel: SKLabelNode!
    var wallPair = SKNode()
    var moveAndRemove = SKAction()
    var gameStarted = Bool()
    var score = Int()
    var died = Bool()
    var restartButton = SKSpriteNode()
    
    func restartScene() {
        
        self.removeAllChildren()
        self.removeAllActions()
        died = false
        gameStarted = false
        score = 0
        createScene()
    }
    
    func createScene() {
        
        let background = SKSpriteNode(imageNamed: "sky")
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        addChild(background)
        
        self.physicsWorld.contactDelegate = self
        
        scoreLabel = ScoreLabelNode(frame: self.frame)
        self.addChild(scoreLabel)
        
        ground = GroundNode.createGround(self.frame)
        self.addChild(ground)
        
        bird = BirdNode.createBird(self.frame)
        self.addChild(bird)
    }
    
    override func didMoveToView(view: SKView) {
        
        createScene()
    }
    
    func createButton() {
        
        restartButton = SKSpriteNode(color: SKColor.grayColor(), size: CGSize(width: 200, height: 100))
        restartButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        restartButton.zPosition = 6
        
        let nameLabel = SKLabelNode()
        nameLabel.text = "Restart"
        restartButton.addChild(nameLabel)
        nameLabel.verticalAlignmentMode = .Center
        nameLabel.horizontalAlignmentMode = .Center
        
        let screenNode = SKSpriteNode(color: UIColor.clearColor(), size: self.size)
        screenNode.anchorPoint = CGPoint(x: 0, y: 0)
        addChild(screenNode)
        
        self.addChild(restartButton)
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        let firstBody = contact.bodyA
        let secondBody = contact.bodyB
        
        if firstBody.categoryBitMask == PhysicsCategory.Score && secondBody.categoryBitMask == PhysicsCategory.Bird || firstBody.categoryBitMask == PhysicsCategory.Bird && secondBody.categoryBitMask == PhysicsCategory.Score {
            
            score++
            scoreLabel.updateScore(score)
        }
        
        if firstBody.categoryBitMask == PhysicsCategory.Bird && secondBody.categoryBitMask == PhysicsCategory.Wall || firstBody.categoryBitMask == PhysicsCategory.Wall && secondBody.categoryBitMask == PhysicsCategory.Bird {
            
            died = true
            createButton()
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if gameStarted == false {
            
            gameStarted = true
            
            bird.physicsBody?.affectedByGravity = true
            
            let spawn = SKAction.runBlock({
                () in
                
                self.createWalls()
            })
            
            let delay = SKAction.waitForDuration(2.0)
            let spawnDelay = SKAction.sequence([spawn, delay])
            let spawnDelayForever = SKAction.repeatActionForever(spawnDelay)
            self.runAction(spawnDelayForever)
            
            let distance = CGFloat(self.frame.width + wallPair.frame.width)
            let movePipes = SKAction.moveByX(-distance, y: 0.0, duration: NSTimeInterval(0.01 * distance))
            let removePipes = SKAction.removeFromParent()
            moveAndRemove = SKAction.sequence([movePipes, removePipes])
            
            bird.physicsBody?.velocity = CGVectorMake(0, 0)
            bird.physicsBody?.applyImpulse(CGVectorMake(0, 90))
        } else {
            
            if died == true {
                
            } else {
            runAction(SKAction.playSoundFileNamed("Dove.mp3", waitForCompletion: true))
            bird.physicsBody?.velocity = CGVectorMake(0, 0)
            bird.physicsBody?.applyImpulse(CGVectorMake(0, 90))
            }
        }
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            
            if died == true {
                if restartButton.containsPoint(location) {
                    
                    restartScene()
                }
                
            }
        }
    }
    
    func createWalls() {
        
        let scoreNode = ScoreNode.createScoreNode(self.frame)
        let topWall = TopWallNode.createTopWallNode(self.frame)
        let bottomWall = BottomWallNode.createBottomNode(self.frame)
        
        wallPair = SKNode()
        wallPair.addChild(topWall)
        wallPair.addChild(bottomWall)
        wallPair.zPosition = 1
        
        let randomPosition = CGFloat.random(min: -200, max: 200)
        wallPair.position.y = wallPair.position.y + randomPosition
        wallPair.addChild(scoreNode)
        wallPair.runAction(moveAndRemove)
        
        self.addChild(wallPair)
    }
}