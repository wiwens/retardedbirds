//
//  BottomWallNode.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 11.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class BottomWallNode {
    
    class func createBottomNode(frame: CGRect) -> SKSpriteNode {
        
        let bottomWall = SKSpriteNode(imageNamed: "Wall")
        bottomWall.position = CGPoint(x: frame.width, y: frame.height / 2 - 350)
        bottomWall.setScale(0.5)
        bottomWall.physicsBody = SKPhysicsBody(rectangleOfSize: bottomWall.size)
        bottomWall.physicsBody?.categoryBitMask = PhysicsCategory.Wall
        bottomWall.physicsBody?.collisionBitMask = PhysicsCategory.Bird
        bottomWall.physicsBody?.contactTestBitMask = PhysicsCategory.Bird
        bottomWall.physicsBody?.affectedByGravity = false
        bottomWall.physicsBody?.dynamic = false
        
        return bottomWall
    }
}