//
//  ScoreNode.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 11.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class ScoreNode {
    
    class func createScoreNode(frame: CGRect) -> SKSpriteNode {
        
        let scoreNode = SKSpriteNode()
        scoreNode.size = CGSize(width: 1, height: 200)
        scoreNode.position = CGPoint(x: frame.width, y: frame.height / 2)
        scoreNode.physicsBody = SKPhysicsBody(rectangleOfSize: scoreNode.size)
        scoreNode.physicsBody?.affectedByGravity = false
        scoreNode.physicsBody?.dynamic = false
        scoreNode.physicsBody?.categoryBitMask = PhysicsCategory.Score
        scoreNode.physicsBody?.collisionBitMask = 0
        scoreNode.physicsBody?.contactTestBitMask = PhysicsCategory.Bird
//        scoreNode.color = SKColor.blueColor()

        return scoreNode
    }
}
