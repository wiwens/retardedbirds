//
//  RandomizerUtilities.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 10.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import Foundation
import CoreGraphics

public extension CGFloat {
    
    public static func random () -> CGFloat {
        
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    public static func random(min min: CGFloat, max: CGFloat) -> CGFloat {
        
        return CGFloat.random() * (max - min) + min
    }
}