//
//  TopWallNode.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 11.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class TopWallNode {
    
    class func createTopWallNode(frame: CGRect) -> SKSpriteNode {
        
        let topWall =  SKSpriteNode(imageNamed: "Wall")
        topWall.position = CGPoint(x: frame.width, y: frame.height / 2 + 350)
        topWall.setScale(0.5)
        topWall.physicsBody = SKPhysicsBody(rectangleOfSize: topWall.size)
        topWall.physicsBody?.categoryBitMask = PhysicsCategory.Wall
        topWall.physicsBody?.collisionBitMask = PhysicsCategory.Bird
        topWall.physicsBody?.contactTestBitMask = PhysicsCategory.Bird
        topWall.physicsBody?.affectedByGravity = false
        topWall.physicsBody?.dynamic = false
        topWall.zRotation = CGFloat(M_PI)
        
        return topWall
    }
}
