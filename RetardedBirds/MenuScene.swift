//
//  MenuScene.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 10.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class MenuScene: SKScene {
    
        var restartButton = SKSpriteNode()
//    var playButton = SKSpriteNode()
//    let playButtonTex = SKTexture(imageNamed: "Ghost")
//    
//    override func didMoveToView(view: SKView) {
//        /* Setup your scene here */
//        
//        playButton = SKSpriteNode(texture: playButtonTex)
//        playButton.position = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2)
//        self.addChild(playButton)
//    }
//    
//    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
//        /* Called when a touch begins */
//        
//        for touch in touches {
//            
//            let location = touch.locationInNode(self)
//
//            if playButton.containsPoint(location) {
//                
//                if let view = view {
//                    let scene = GameScene(fileNamed:"GameScene")
//                    scene?.scaleMode = SKSceneScaleMode.AspectFill
//                    view.presentScene(scene)
//                }
//            }
//        }
//    }
    override func didMoveToView(view: SKView) {
        addButtons()
    }
    
    private func addButtons() {
        restartButton = SKSpriteNode(color: SKColor.redColor(), size: CGSize(width: 200, height: 100))
        restartButton.position = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        restartButton.zPosition = 6
        
        let nameLabel = SKLabelNode()
        nameLabel.text = "Start Game"
        nameLabel.color = SKColor.yellowColor()
        restartButton.addChild(nameLabel)
        nameLabel.verticalAlignmentMode = .Center
        nameLabel.horizontalAlignmentMode = .Center
        
        self.addChild(restartButton)
    }
    
    private func startGame() {
        print("touched")
//        if let scene = MenuScene(fileNamed: "MenuScene") {
//            // Configure the view.
//            let skView = self.view! as SKView
//            skView.showsFPS = true
//            skView.showsNodeCount = true
//            
//            /* Sprite Kit applies additional optimizations to improve rendering performance */
//            skView.ignoresSiblingOrder = true
//            
//            /* Set the scale mode to scale to fit the window */
//            scene.scaleMode = .AspectFill
//            
//            skView.presentScene(scene)
//        }
//
//        let skView = self.view as! SKView
//        skView.showsFPS = true
//        skView.showsNodeCount = true
//        
//        /* Sprite Kit applies additional optimizations to improve rendering performance */
//        skView.ignoresSiblingOrder = true
//        
//        /* Set the scale mode to scale to fit the window */
//        scene.scaleMode = .AspectFill
//        
//        skView.presentScene(scene)

        
        let gameScene = GameScene(size: view!.bounds.size)
        gameScene.scaleMode = .AspectFill
        let transition = SKTransition.fadeWithDuration(0.15)
        view!.presentScene(gameScene, transition: transition)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        for touch in touches {
            
            let location = touch.locationInNode(self)
            
            if restartButton.containsPoint(location) {
                

                    startGame()

            }
        }
    }

}
