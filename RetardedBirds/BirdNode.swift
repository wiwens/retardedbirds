//
//  BirdNode.swift
//  RetardedBirds
//
//  Created by Wiktor Wielgus on 11.01.2016.
//  Copyright © 2016 Wiktor. All rights reserved.
//

import SpriteKit

class BirdNode {

    class func createBird(frame: CGRect) -> SKSpriteNode {
        
        let bird = SKSpriteNode(imageNamed: "Bird")
        bird.size = CGSize(width: 60, height: 70)
        bird.position = CGPoint(x: frame.width / 2 - bird.frame.width, y: frame.height / 2)
        bird.physicsBody = SKPhysicsBody(circleOfRadius: bird.frame.height / 2)
        bird.physicsBody?.categoryBitMask = PhysicsCategory.Bird
        bird.physicsBody?.collisionBitMask = PhysicsCategory.Ground | PhysicsCategory.Wall
        bird.physicsBody?.contactTestBitMask = PhysicsCategory.Ground | PhysicsCategory.Wall | PhysicsCategory.Score
        bird.physicsBody?.affectedByGravity = false
        bird.physicsBody?.dynamic = true
        bird.zPosition =  2
        
        return bird
    }
}

